##  deployed

####    React apps

[Cabal (Haskell package manager) info site](https://ciezbit.bitbucket.io/cabal/)

[Bible in Latvian](http://bibleja.lv/)

[Calculator](https://ciezbit.bitbucket.io/calc/)


####    Angular apps

[editable tree](https://ciezbit.bitbucket.io/angular-tree/)


##  sources

####    Javascript, C#    

[bitbucket](https://bitbucket.org/ciezbit/)


####    Haskell

[hackage overview](https://hackage.haskell.org/user/procione)

[list -> tree algo](https://hackage.haskell.org/package/elenco-albero)

[date, time data structures, transformations](https://hackage.haskell.org/package/hora)

[PCRE regex convenience library](https://hackage.haskell.org/package/regex-do)

[distributed P2P app](https://hackage.haskell.org/package/raketka)


##  activity in a programming forum

[IntelliJ plugin development forum](https://intellij-support.jetbrains.com/hc/en-us/profiles/1373847991-Imants-Cekusins)

